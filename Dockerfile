FROM golang:alpine as build

WORKDIR /go/src/badge-manager

ENV GOPATH="/go"

COPY . .

RUN go mod download

RUN go build -i -o ./server .

FROM alpine

EXPOSE 8082

COPY --from=build /go/src/badge-manager/server .

# Run executable
ENTRYPOINT ["./server"]

