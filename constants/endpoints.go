package constants

const (
	ApiHost          = "API_HOST"
	ApiRefHost       = "API_REFERENCE_HOST"
	ApiDocHost       = "API_DOC_HOST"
	MongoExpressHost = "MONGO_EXPRESS_HOST"
)
